<?php

declare(strict_types = 1);

namespace Drupal\recurring_task\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\recurring_task\Entity\RecurringTaskRunnerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the action controller.
 */
class RecurringTaskActionController extends ControllerBase {

  /**
   *
   */
  public function action(string $operation, RecurringTaskRunnerInterface $runner): RedirectResponse {
    \assert(in_array($operation, ['start', 'stop'], TRUE));

    $args = ['%runner' => $runner->label()];
    if ($operation === 'start') {
      if ($runner->start()) {
        $this->messenger()->addStatus($this->t('Successfully started the %runner recurring task', $args));
      }
      else {
        $this->messenger()->addError($this->t('Failed to start the %runner recurring task', $args));
      }
    }
    else {
      if ($runner->stop()) {
        $this->messenger()->addStatus($this->t('Successfully stopped the %runner recurring task', $args));
      }
      else {
        $this->messenger()->addError($this->t('Failed to stop the %runner recurring task', $args));
      }
    }

    return $this->redirect('entity.recurring_task_runner.collection');
  }

}
