<?php

/**
 * @file
 * Recurring task command script.
 */

declare(strict_types = 1);

use Drupal\Component\Serialization\Json;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\Core\Site\Settings;
use Psr\Log\LogLevel;
use React\ChildProcess\Process;
use React\EventLoop\Loop;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

// Note that this file is not accessible via webserver as the access is
// forbidden from the webroot .htaccess file. For any case we check that the
// script is called only via CLI.
if (PHP_SAPI !== 'cli' || $argc !== 2) {
  throw new \Exception('Invalid usage');
}

$autoloader = require $_SERVER['PWD'] . '/../vendor/autoload.php';

// Unpack passed arguments.
$params = Json::decode(base64_decode($argv[1]));
extract($params);

if (!is_string($command) || !is_int($interval) || !is_bool($debug)) {
  throw new \InvalidArgumentException('Invalid usage');
}

// Create a fake request as Drupal kernel is an HTTP kernel.
$request = Request::createFromGlobals();
$request->attributes->set(RouteObjectInterface::ROUTE_OBJECT, new Route('<none>'));
$request->attributes->set(RouteObjectInterface::ROUTE_NAME, '<none>');

// Load Drupal kernel.
$kernel = new DrupalKernel('prod', $autoloader);
$kernel::bootEnvironment();
$kernel->setSitePath('sites/default');
Settings::initialize($kernel->getAppRoot(), $kernel->getSitePath(), $autoloader);
$kernel->boot();
$kernel->preHandle($request);

$loop = Loop::get();
$loop->addPeriodicTimer($interval, function () use ($command) {
  try {
    $process = new Process($command);
    $process->start();

    $process->on('exit', function ($exitCode, $term) use ($command) {
      if ($term === NULL) {
        if ($exitCode > 0) {
          drupalLog(LogLevel::ERROR, "Exited with code $exitCode ($command)");
        }
        else {
          drupalLog(LogLevel::DEBUG, "Successfully ran $command");
        }
      }
      else {
        drupalLog(LogLevel::ERROR, "Terminated with signal $term ($command)");
      }
    });
    $process->stdout->on('data', function ($chunk) use ($command) {
      drupalLog(LogLevel::DEBUG, "STDOUT on data: $chunk ($command)");
    });
    $process->stderr->on('data', function ($chunk) use ($command) {
      drupalLog(LogLevel::DEBUG, "STDERR on data: $chunk ($command)");
    });
    $process->stdout->on('error', function (\Exception $e) use ($command) {
      drupalLog(LogLevel::DEBUG, "STDOUT on error: {$e->getMessage()} ($command)");
    });
    $process->stderr->on('error', function (\Exception $e) use ($command) {
      drupalLog(LogLevel::DEBUG, "STDERR on error: {$e->getMessage()} ($command)");
    });
  }
  catch (\Exception $e) {
    drupalLog(LogLevel::ERROR, $e->getMessage());
  }
});
$loop->run();

/**
 * Logs a message to Drupal logger.
 *
 * @param string $level
 *   Message level.
 * @param string $message
 *   The message.
 */
function drupalLog(string $level, string $message): void {
  // Debug messages are logged only when debug is required.
  if (!$GLOBALS['debug'] && $level === LogLevel::DEBUG) {
    return;
  }
  \Drupal::logger('recurring_task')->log($level, $message);
}
