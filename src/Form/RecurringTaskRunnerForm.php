<?php

declare(strict_types = 1);

namespace Drupal\recurring_task\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\recurring_task\Entity\RecurringTaskRunner;
use Drupal\recurring_task\Entity\RecurringTaskRunnerInterface;
use PhpParser\Node\Stmt\InlineHTML;
use PhpParser\ParserFactory;
use Symfony\Component\Filesystem\Path;

/**
 * Form for 'recurring_task_runner' entities.
 */
class RecurringTaskRunnerForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $formState) {
    $form = parent::form($form, $formState);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->getEntity()->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->getEntity()->id(),
      '#machine_name' => [
        'exists' => RecurringTaskRunner::class . '::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$this->getEntity()->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->getEntity()->get('description'),
    ];

    $form['command'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Command'),
      '#description' => $this->t("Command as an absolute path. The <code>[drupal:root]</code> token can be used to designate the Drupal webroot"),
      '#default_value' => $this->getEntity()->get('command'),
      '#required' => TRUE,
    ];

    $form['params'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Command line parameters'),
      '#description' => $this->t("Parameters and options to be appended to the command. The <code>[drupal:root]</code> and <code>[drupal:url]</code> tokens can be used to designate the Drupal webroot, respectively the site's URL"),
      '#default_value' => $this->getEntity()->get('params'),
    ];

    $form['interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Interval'),
      '#description' => $this->t('Seconds between two consecutive recurring tasks'),
      '#default_value' => $this->getEntity()->get('interval'),
      '#min' => 1,
    ];

    $form['autoRestart'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto restart'),
      '#description' => $this->t('Automatically restart the runner after a failure, such as a server reboot. If checked, the task will automatically restart on next cron run'),
      '#default_value' => $this->getEntity()->get('autoRestart'),
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('If checked, useful debug information are sent to Drupal log'),
      '#default_value' => $this->getEntity()->get('debug'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState): void {
    parent::validateForm($form, $formState);

    // Absolute path?
    $command = Path::canonicalize(trim(str_replace('[drupal:root]', DRUPAL_ROOT, $formState->getValue('command'))));
    if (!Path::isAbsolute($command)) {
      $formState->setErrorByName('command', $this->t('The command should have an absolute path or should be prefixed with the <code>[drupa:root]</code> token'));
      return;
    }
    // Does the file exists?
    if (!file_exists($command) || (filesize($command) === 0)) {
      $formState->setErrorByName('command', $this->t("The PHP @file doesn't exist or it's empty", [
        '@file' => $command,
      ]));
      return;
    }
    // Is it a PHP file?
    $parser = (new ParserFactory())->create(ParserFactory::PREFER_PHP7);
    $parsed = $parser->parse(file_get_contents($command));
    if (count($parsed) === 1 && $parsed[0] instanceof InlineHTML) {
      $formState->setErrorByName('command', $this->t("The @file file doesn't contain any PHP code", [
        '@file' => $command,
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState): void {
    // Properly cast booleans.
    $formState->setValue('autoRestart', (bool) $formState->getValue('autoRestart'));
    $formState->setValue('debug', (bool) $formState->getValue('debug'));

    parent::submitForm($form, $formState);

    $formState->setRedirect('entity.recurring_task_runner.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity(): RecurringTaskRunnerInterface {
    // Overrides the parent just for the sake of a fancy return typing.
    /** @var \Drupal\recurring_task\Entity\RecurringTaskRunnerInterface $entity */
    $entity = parent::getEntity();
    return $entity;
  }

}
