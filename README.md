# Async Recurring Tasks

This module has been heavily inspired by [Matt Glaman](
https://www.drupal.org/u/mglaman) blog post [Using ReactPHP to run Drupal tasks
](https://mglaman.dev/blog/using-reactphp-run-drupal-tasks).

The module allows to configure recurring runners that will run on specific
intervals. A runner consists of a command, actually PHP file and its command
line parameters. For each runner, the site builder should specify a running
interval in seconds.

A runner can be restarted automatically, on cron, after an accidental stop, such
as a server or services reboot.

The module has no Drupal dependencies but depends on ReactPHP.
