<?php

declare(strict_types = 1);

namespace Drupal\recurring_task\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\recurring_task\ProcessManager;
use Symfony\Component\Filesystem\Path;

/**
 * Defines the 'recurring_task_runner' config entities.
 *
 * @ConfigEntityType(
 *   id = "recurring_task_runner",
 *   label = @Translation("Task runner"),
 *   label_collection = @Translation("Task runners"),
 *   label_singular = @Translation("task runner"),
 *   label_plural = @Translation("task runners"),
 *   label_count = @PluralTranslation(
 *     singular = "@count task runner",
 *     plural = "@count task runners",
 *   ),
 *   handlers = {
 *     "access" = "\Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\recurring_task\Form\RecurringTaskRunnerForm",
 *       "edit" = "Drupal\recurring_task\Form\RecurringTaskRunnerForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\recurring_task\Entity\RecurringTaskRunnerListBuilder",
 *   },
 *   admin_permission = "administer recurring task",
 *   config_prefix = "runner",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/system/recurring-task/add",
 *     "edit-form" = "/admin/config/system/recurring-task/manage/{recurring_task_runner}",
 *     "delete-form" = "/admin/config/system/recurring-task/manage/{recurring_task_runner}/delete",
 *     "collection" = "/admin/config/system/recurring-task",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "command",
 *     "params",
 *     "interval",
 *     "autoRestart",
 *     "debug",
 *   }
 * )
 */
class RecurringTaskRunner extends ConfigEntityBase implements RecurringTaskRunnerInterface {

  /**
   * The entity ID.
   */
  protected string $id;

  /**
   * The entity label.
   */
  protected string $label;

  /**
   * The optional entity description.
   */
  protected string $description = '';

  /**
   * Command to run.
   */
  protected string $command;

  /**
   * The command line parameters.
   */
  protected string $params = '';

  /**
   * Seconds between two runs.
   */
  protected int $interval = 60;

  /**
   * Whether to automatically restart the recurring task in case of failure.
   */
  protected bool $autoRestart = FALSE;

  /**
   * Whether to run in debug mode.
   */
  protected bool $debug = FALSE;

  /**
   * {@inheritdoc}
   */
  public function start(): bool {
    $argument = base64_encode(Json::encode([
      'command' => $this->getCommand(),
      'interval' => $this->interval,
      'debug' => $this->debug,
    ]));
    $process = new ProcessManager();
    $pid = $process->start('php ' . realpath(__DIR__ . '/../../script/run.php') . ' ' . $argument)->getPid();
    if ($status = $process->isRunning($pid)) {
      $this->registerPid($pid);
    }
    else {
      $this->unregisterPid();
    }
    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function stop(): bool {
    if (!$pid = $this->getRegisteredPid()) {
      return TRUE;
    }
    $process = new ProcessManager();
    $process->setPid($pid)->stop();

    if (!$process->isRunning($pid)) {
      // The process has been stopped successfully.
      $this->unregisterPid();
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommand(): string {
    $command = Path::canonicalize(trim(str_replace('[drupal:root]', DRUPAL_ROOT, $this->command)));
    $params = trim(strtr($this->params, [
      '[drupal:root]' => DRUPAL_ROOT,
      '[drupal:url]' => $GLOBALS['base_url'],
    ]));
    return $command . ' ' . $params;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegisteredPid(): ?int {
    return $this->getRegisteredProcessInfo('pid');
  }

  /**
   * {@inheritdoc}
   */
  public function getStartTime(): ?int {
    return $this->getRegisteredProcessInfo('started');
  }

  /**
   * {@inheritdoc}
   */
  public function isRunning(int $pid): bool {
    return (new ProcessManager())->isRunning($pid);
  }

  /**
   * Returns process data registered for this runner, if any.
   *
   * @param string $type
   *   Data type ('pid' or 'started').
   *
   * @return int|null
   *   The specific data to retrieve based on the requested $type:
   *   - pid: The process ID (pid).
   *   - started: The process start time as a Unix timestamp.
   */
  protected function getRegisteredProcessInfo(string $type): ?int {
    \assert(in_array($type, ['pid', 'started'], TRUE));
    $processes = \Drupal::state()->get('recurring_task.processes', []);
    if (!isset($processes[$this->id()])) {
      return NULL;
    }
    return $processes[$this->id][$type];
  }

  /**
   * Registers the process ID (pid) in the backend.
   *
   * @param int $pid
   *   The process ID.
   */
  protected function registerPid(int $pid): void {
    $state = \Drupal::state();
    $processes = $state->get('recurring_task.processes', []);
    $processes[$this->id()] = ['pid' => $pid, 'started' => time()];
    $state->set('recurring_task.processes', $processes);
  }

  /**
   * Unregisters the process ID (pid) from the backend.
   */
  protected function unregisterPid(): void {
    $state = \Drupal::state();
    $processes = $state->get('recurring_task.processes', []);
    unset($processes[$this->id()]);
    if ($processes) {
      $state->set('recurring_task.processes', $processes);
    }
    else {
      $state->delete('recurring_task.processes');
    }
  }

}
