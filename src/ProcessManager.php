<?php

declare(strict_types = 1);

namespace Drupal\recurring_task;

/**
 * Helper for starting and stopping a process.
 */
class ProcessManager {

  /**
   * The process ID (pid).
   */
  private ?int $pid = NULL;

  /**
   * Starts the runner.
   *
   * @param string $command
   *   The command, including the command line parameters.
   *
   * @return $this
   */
  public function start(string $command): self {
    $command = "nohup " . $command . ' > /dev/null 2>&1 & echo $!';
    exec($command, $op);
    $this->pid = (int) $op[0];
    return $this;
  }

  /**
   * Stops the current process.
   *
   * @return $this
   */
  public function stop(): self {
    exec("kill $this->pid");
    $this->pid = NULL;
    return $this;
  }

  /**
   * Returns the current process ID (pid) if any.
   *
   * @return int|null
   *   The process ID (pid) or NULL.
   */
  public function getPid(): ?int {
    return $this->pid;
  }

  /**
   * Sets the process ID (pid).
   *
   * @param int $pid
   *   The process ID (pid).
   *
   * @return $this
   */
  public function setPid(int $pid): self {
    $this->pid = $pid;
    return $this;
  }

  /**
   * Checks if a process is running.
   *
   * @return bool
   *   Whether the process is currently running.
   */
  public function isRunning(int $pid): bool {
    // Check if the ps command is provided by BusyBox. BusyBox's ps does not
    // have a -p option so check if the process exists using procfs instead. The
    // BusyBox hack is borrowed from `advancedqueue_runner` module.
    $return = exec('exec 2>/dev/null; readlink "/bin/ps"');
    if ($return && preg_match('/^.*\/busybox$/', $return)) {
      exec("test -h /proc/$pid/exe", $op, $rstat);
      return $rstat === 0;
    }
    exec("ps -p $pid", $op);
    return isset($op[1]);
  }

}
