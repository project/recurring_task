<?php

declare(strict_types = 1);

namespace Drupal\recurring_task\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder for 'recurring_task_runner' entities.
 */
class RecurringTaskRunnerListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'label' => $this->t('Runner'),
      'status' => $this->t('Status'),
      'started' => $this->t('Started'),
      'command' => $this->t('Command'),
      'interval' => $this->t('Interval'),
      'auto_restart' => $this->t('Auto-restart'),
      'debug' => $this->t('Debug mode'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    \assert($entity instanceof RecurringTaskRunnerInterface);

    if ($pid = $entity->getRegisteredPid()) {
      if ($entity->isRunning($pid)) {
        $status = $this->t('running (pid @pid)', ['@pid' => $pid]);
      }
      else {
        $status = $this->t('dead (pid @pid)', ['@pid' => $pid]);
      }
    }
    else {
      $status = $this->t('idle');
    }

    if ($timestamp = $entity->getStartTime()) {
      $started = \Drupal::getContainer()->get('date.formatter')->format($timestamp, 'custom', 'Y-m-d H:i');
    }

    return [
      'label' => $entity->label(),
      'status' => $status,
      'started' => $started ?? '',
      'command' => [
        'data' => [
          '#markup' => '<code>' . $entity->getCommand() . '</code>',
        ],
      ],
      'interval' => $entity->get('interval'),
      'auto_restart' => $entity->get('autoRestart') ? $this->t('yes') : $this->t('no'),
      'debug' => $entity->get('debug') ? $this->t('yes') : $this->t('no'),
    ] + parent::buildRow($entity);
  }

}
