<?php

declare(strict_types = 1);

namespace Drupal\recurring_task\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for 'recurring_task_runner' entity type.
 */
interface RecurringTaskRunnerInterface extends ConfigEntityInterface {

  /**
   * Starts the async recurring task.
   *
   * @return bool
   *   Whether the operation has been successful.
   */
  public function start(): bool;

  /**
   * Stops the async recurring task.
   *
   * @return bool
   *   Whether the operation has been successful.
   */
  public function stop();

  /**
   * Returns the command, including the command parameters and options.
   *
   * @return string
   *   The complete command.
   */
  public function getCommand(): string;

  /**
   * Returns the process ID (pid) from database or NULL if none.
   *
   * @return int|null
   *   The process ID (pid) or NULL.
   */
  public function getRegisteredPid(): ?int;

  /**
   * Returns the time when the process has been started as a Unix timestamp.
   *
   * @return int|null
   *   The time when the process has been started as a Unix timestamp.
   */
  public function getStartTime(): ?int;

  /**
   * Checks whether a process with a given ID is running.
   *
   * @param int $pid
   *   The process ID (pid).
   *
   * @return bool
   *   Whether a process with a given ID is running.
   */
  public function isRunning(int $pid): bool;

}
